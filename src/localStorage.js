
function Storage(prefix) {
    let storage = {
        getItem: function(name) {
            return JSON.parse(window.sessionStorage.getItem(prefix + '.' + name));
        },
        setItem: function(name, value) {
            window.sessionStorage.setItem(prefix + '.' + name, JSON.stringify(value));
        }
    };
    const keys = {
        product: 'products',
        category: 'categories'
    };
    return {
        addProduct: function(product) {
            product.id = guid();
            var products = storage.getItem(keys.product);
            if (!products) {
                products = [product];
            } else {
                products.push(product);
            }
            try {
                storage.setItem(keys.product, products);
            } catch (e) {
                console.error('storage err', e);
            }
            return products;
        },
        removeProduct: function(id) {
            let products = storage.getItem(keys.product);
            var index = products.findIndex(function(item) {
                return item.id === id;
            });
            if (index => 0) {
                products.splice(index, 1);
            }
            storage.setItem(keys.product, products);
            return products;
        },
        getProducts: function() {
            return storage.getItem(keys.product);
        }
    }
}

// https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

export default Storage;
