import React from 'react';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';
import {
    Nav, NavItem
} from 'react-bootstrap';
import {
    LinkContainer
} from 'react-router-bootstrap';
import logo from './logo.svg';
import './App.css';
import Home from './home';
import Product from './product';

const App = () => (
    <Router>
        <div className="App">
            <Nav bsStyle="pills">
                <LinkContainer to="/home"><NavItem href="/home">Homie</NavItem></LinkContainer>
                <LinkContainer to="/product"><NavItem href="/product">Products</NavItem></LinkContainer>
            </Nav>
            <div className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h2>Welcome to React</h2>
            </div>
            <p className="App-intro">
                To get started, edit <code>src/App.js</code> and save to reload.
            </p>

            <hr/>

            <Route exact path="/" component={Home}/>
            <Route path="/product" component={Product}/>
        </div>
    </Router>
);
export default App