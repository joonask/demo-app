import React from 'react';
import {FormGroup, ControlLabel, FormControl, Button} from 'react-bootstrap';
import Storage from '../localStorage';

let myStorage = Storage('myStorage');

class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {products: []};
        this.onSave = this.onSave.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }
    componentDidMount() {
        this.setState({products: getProducts()});
    }
    onSave(product) {
        console.log('Product onSave action', product);
        myStorage.addProduct(product);
        this.setState({products: getProducts()});
    }
    onDelete(id) {
        console.log('Product.onDelete', id);
        this.setState({products: myStorage.removeProduct(id)});
    }
    render() {
        return (
            <div>
                <h3>Product page</h3>
                <AddProduct onSave={this.onSave}></AddProduct>
                <ListProducts onDelete={this.onDelete} products={this.state.products}></ListProducts>
            </div>
        );
    }
}

class AddProduct extends React.Component {
    constructor(props) {
        super(props);
        this.save = this.save.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.state = {name: '', category: ''};
    }
    save() {
        console.log('AddProduct.save', this.state);
        this.props.onSave(this.state);
        this.resetForm();
    }
    resetForm() {
        this.setState({name: '', category: ''});
    }
    handleTextChange(e) {
        this.setState({
            name: e.target.value,
            category: this.state.category
        });
    }
    handleSelect(e) {
        this.setState({
            name: this.state.name,
            category: e.target.value
        });
    }
    render() {
        return (
            <form>
                <FormGroup>
                    <ControlLabel>Name:</ControlLabel>
                    <FormControl type="text" placeholder="set name" value={this.state.name} onChange={this.handleTextChange}/>
                    <ControlLabel>Category:</ControlLabel>
                    <FormControl componentClass="select" value={this.state.category} onChange={this.handleSelect}>
                        <option value="select">select some item</option>
                        <option value="other">otjer</option>
                        <option value="other2">categories todo</option>
                    </FormControl>
                    <Button onClick={this.save}>Save</Button>
                </FormGroup>
            </form>
        );
    }
}
const ListProducts  = (props) => {
    return (
        <div className="products">
            <ProductHeader/>
            <ProductList onDelete={props.onDelete} products={props.products}/>
        </div>
    );
};
const ProductHeader = () => (
    <div className="row product-header">
        <div className="col-lg-4">ID</div>
        <div className="col-lg-3">Name</div>
        <div className="col-lg-4">Category</div>
        <div className="col-lg-1"></div>
    </div>
);
const ProductList = (props) => {
    const deleteItem = (product) => () => {
        props.onDelete(product.id);
    };
    const listItems = props.products.map((product) =>
        <div className="row" key={product.id}>
            <div className="col-lg-4">{product.id}</div>
            <div className="col-lg-3">{product.name}</div>
            <div className="col-lg-4">{product.category}</div>
            <div className="col-lg-1"><span onClick={deleteItem(product)} className="glyphicon glyphicon-remove"></span></div>
        </div>
    );
    return (
        <div className="row">{listItems}</div>
    );
};
const getProducts = () => {
    let products = myStorage.getProducts();
    return products || [];
};

export default Product;